#pragma once

#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <robot_interaction_tools_msgs/GetRobotState.h>
#include <robot_interaction_tools_msgs/SendStoredPose.h>

#include <robot_interaction_tools_msgs/PlanJointAction.h>
#include <robot_interaction_tools_msgs/PlanCartesianAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>


#include <Eigen/Geometry>

/**
 * @class TestingTracikSearch
 */
class TestingTracikSearch {
 public:
  TestingTracikSearch( ros::NodeHandle &_nh,
		       const std::string &_traj_cmd_topic = "/joint_path_command" );
  bool move_directly_to_joint_pose( Eigen::VectorXd _pose );
  void turn_collision( bool _on );
  bool send_stored_pose( std::string _group, std::string _name );
  bool get_current_pose( sensor_msgs::JointState &_current_pose,
			 std::string _group_name );
  bool move_cartesian( std::string _group,
		       geometry_msgs::PoseStamped _pose );
 protected:
  ros::NodeHandle nh_;
  ros::ServiceClient srv_get_robot_state_;
  ros::ServiceClient srv_send_stored_pose_;
  actionlib::SimpleActionClient<robot_interaction_tools_msgs::PlanCartesianAction>* client_cartesian_;

  
  
  std::string topic_traj_cmd_;
  ros::Publisher pub_traj_cmd_;
};
