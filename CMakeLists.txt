cmake_minimum_required(VERSION 2.8.3)
project(craftsman_testing)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  planner_interface
  robot_interaction_tools_msgs
  trajectory_msgs
  roscpp
  interactive_markers
)


###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES craftsman_testing
  CATKIN_DEPENDS  planner_interface robot_interaction_tools_msgs trajectory_msgs roscpp interactive_markers
#  DEPENDS system_lib
)

#################################
find_package(Bullet COMPONENTS BulletMath BulletCollision MODULE QUIET)
if(BULLET_FOUND)
  # Test whether Bullet was built with double precision. If so, we need to
  # define the BT_USE_DOUBLE_PRECISION pre-processor directive before including
  # any Bullet headers. This is a workaround for the fact that Bullet does not
  # add the definition to BULLET_DEFINITIONS or generate a #cmakedefine header.
  include(CheckCXXSourceCompiles)
  set(CMAKE_REQUIRED_FLAGS "")
  set(CMAKE_REQUIRED_DEFINITIONS "-DBT_USE_DOUBLE_PRECISION")
  set(CMAKE_REQUIRED_INCLUDES "${BULLET_INCLUDE_DIRS}")
  set(CMAKE_REQUIRED_LIBRARIES "${BULLET_LIBRARIES}")
  check_cxx_source_compiles(
    "
    include <btBulletCollisionCommon.h>
    int main()
    {
      btVector3 v(0., 0., 1.);
      btStaticPlaneShape planeShape(v, 0.);
      return 0;
    }
    "
    BT_USE_DOUBLE_PRECISION
  )

  #if(DART_VERBOSE)
    if(BT_USE_DOUBLE_PRECISION)
      message(STATUS "Looking for Bullet - found (double precision)")
    else()
      message(STATUS "Looking for Bullet - found (single precision)")
    endif()
  #endif()

  #set(HAVE_BULLET TRUE CACHE BOOL "Check if BULLET found." FORCE)
#else()
#  message(STATUS "Looking for Bullet - NOT found, to use dart-collision-bullet, please install libbullet-dev")
#  set(HAVE_BULLET FALSE CACHE BOOL "Check if BULLET found." FORCE)
#  return()
endif()
###################################


###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
 include
 ${catkin_INCLUDE_DIRS}
 ${BULLET_INCLUDE_DIRS}
)

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
add_executable( test_padding_params_and_services src/test_padding_params_and_services.cpp )
target_link_libraries( test_padding_params_and_services ${catkin_LIBRARIES})

add_library( testing_tracik_search SHARED src/testing_tracik_search.cpp )
target_link_libraries( testing_tracik_search ${catkin_LIBRARIES})


add_executable( testing_tracik_search_node src/testing_tracik_search_node.cpp )
target_link_libraries( testing_tracik_search_node testing_tracik_search ${catkin_LIBRARIES})

add_executable( test_bullet_fcl  src/test_bullet_vs_fcl.cpp )
target_link_libraries( test_bullet_fcl  ${catkin_LIBRARIES}  ${BULLET_LIBRARIES} )


## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_craftsman_testing.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
