#include <craftsman_testing/testing_tracik_search.h>
#include <ros/ros.h>

/****/
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "test_tracik_search" );
  ros::NodeHandle nh;

  TestingTracikSearch ttis(nh);
  ros::spinOnce();
  usleep(0.5*1e6);
  ros::spinOnce();


  // 1. Deactivate collision
  ROS_WARN("Turn collision OFF ");
  ttis.turn_collision( false );
  ros::spinOnce(); usleep(1.0*1e6);
  
  
  // 2. Move arm to home pose
  ROS_WARN("Send arm to Home Position");
  ttis.send_stored_pose("arm", "Home Position");
  ros::spinOnce(); usleep(10.0*1e6);


  // 3. Activate collision again
  ROS_WARN("Turn collision ON ");
  ttis.turn_collision( true );
  ros::spinOnce(); usleep(1.0*1e6);

    
  // 4. Try to move to a hard pose
  ROS_WARN("Move arm to LH_inspection_2 ");

  ttis.send_stored_pose("arm", "LH_inspection_2");
  ros::spinOnce(); usleep(10.0*1e6);

  ROS_WARN("Move back to Home position");
  ttis.send_stored_pose("arm", "Home Position");
  
  // 5. Try again using cartesian search
  
  // 2. Try to go to Goal pose
  
  ros::Rate r(30);
  while( ros::ok() ) {
    ros::spinOnce();
    r.sleep();
  }
  
  return 0;
}

/*
  go_down.header.frame_id = planning_frame_; 
  go_down.header.stamp = ros::Time::now();
  go_down.pose.position.x = msg.transform.translation.x + _dx; // 0.40
  go_down.pose.position.y = msg.transform.translation.y; // 0
  go_down.pose.position.z = msg.transform.translation.z + _dy;  // 1.239; 
  go_down.pose.orientation.x = msg.transform.rotation.x;
  go_down.pose.orientation.y = msg.transform.rotation.y;
  go_down.pose.orientation.z = msg.transform.rotation.z;  
  go_down.pose.orientation.w = msg.transform.rotation.w;
*/
