#include <ros/ros.h>
#include <robot_interaction_tools_msgs/ActivateLinkCollision.h>
#include <robot_interaction_tools_msgs/SendStoredPose.h>
#include <robot_interaction_tools_msgs/SetPose.h>

int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "test_padding_params_and_services" );
  ros::NodeHandle nh;

  // Call service to deactivate collision for parts attached to gripper
  std::vector<std::string> part_links;
  part_links.push_back( "left_raw_part" );
  part_links.push_back( "left_cut_part" );
  part_links.push_back( "right_raw_part" );
  part_links.push_back( "right_cut_part" );

  // 1. Move arm to default pose
  ros::ServiceClient client_1 = nh.serviceClient<robot_interaction_tools_msgs::SendStoredPose>("/interactive_controls/send_stored_pose");
  robot_interaction_tools_msgs::SendStoredPose srv_1;
  srv_1.request.group_name = "arm";
  srv_1.request.pose_name = "LH_inspection_1";

    if (client_1.call(srv_1)) {
      ROS_INFO("Move arm to LH_inspection_1 was SUCCESSFUL" );
    } else {
      ROS_INFO("FAILED moving arm to LH_inspection_1" );
      return 1;
    }

  usleep(2.0*1e6);

   // 2. Try to move griper close to the table, it will be UNREACHABLE since the parts attached to the gripper
   // have collision ON by default
  ros::ServiceClient client_2 = nh.serviceClient<robot_interaction_tools_msgs::SetPose>("/interactive_controls/set_pose");
  robot_interaction_tools_msgs::SetPose srv_2;
  srv_2.request.execute = true;
  srv_2.request.group_name = "arm";
  srv_2.request.pose_stamped.header.frame_id = "base_link";
  srv_2.request.pose_stamped.header.stamp = ros::Time::now();
  srv_2.request.pose_stamped.pose.position.x = 1.770;
  srv_2.request.pose_stamped.pose.position.y = -0.926;
  srv_2.request.pose_stamped.pose.position.z = 0.556;
  srv_2.request.pose_stamped.pose.orientation.x = 0.255;
  srv_2.request.pose_stamped.pose.orientation.y = 0.377;
  srv_2.request.pose_stamped.pose.orientation.z = -0.631;
  srv_2.request.pose_stamped.pose.orientation.w = 0.628;

    if (client_2.call(srv_2)) {
      if( srv_2.response.result ) {
        ROS_INFO("FAILURE: This pose should be UNREACHABLE UNLESS WE DEACTIVATE THE COLLISION OF THE PARTS" );
        return 1;
      } else {
        ROS_INFO("SUCCESS: COLLISION DETECTED before Deactivating parts attached to the gripper" );
      }
    }

  usleep(2.0*1e6);

  // 3. Deactivate collision of gripper parts
  ros::ServiceClient client_3 = nh.serviceClient<robot_interaction_tools_msgs::ActivateLinkCollision>("/planner_node/activate_link_collision");
  robot_interaction_tools_msgs::ActivateLinkCollision srv_3;

  for( int i = 0; i < part_links.size(); ++i ) {
    srv_3.request.link = part_links[i];
    srv_3.request.collision_active = false;
    if (client_3.call(srv_3)) {
      ROS_INFO("SUCCESS: Deactivated collision for link %s", part_links[i].c_str() );
    } else {
      ROS_INFO("FAILED to call service to deactivate collision for link %s ",  part_links[i].c_str() );
    }
  }

  usleep(2.0*1e6);


  // 4. Try again step 3. Now it should be reachable:
  srv_2.request.pose_stamped.header.stamp = ros::Time::now();
    if (client_2.call(srv_2)) {
      if( srv_2.response.result ) {
        ROS_INFO("SUCCESS: After deactivating collision for parts this is reachable!" );
      } else {
        ROS_INFO("FAILURE: This pose should have been reachable by now!" );
        return 1;
      }
    }

  usleep(2.0*1e6);

  // 5. Get the gripper as close as it will get with the current padding of 2 cm:
  srv_2.request.pose_stamped.header.stamp = ros::Time::now();
  srv_2.request.pose_stamped.pose.position.x = 1.751;
  srv_2.request.pose_stamped.pose.position.y = -1.071;
  srv_2.request.pose_stamped.pose.position.z = 0.364;
  srv_2.request.pose_stamped.pose.orientation.x = 0.258;
  srv_2.request.pose_stamped.pose.orientation.y = 0.375;
  srv_2.request.pose_stamped.pose.orientation.z = -0.626;
  srv_2.request.pose_stamped.pose.orientation.w = 0.633;

    if (client_2.call(srv_2)) {
      if( srv_2.response.result ) {
        ROS_INFO("SUCCESS: Gripper is as close as it can get with the current padding of 2cm" );
      } else {
        ROS_INFO("FAILURE: This pose should be REACHABLE" );
        return 1;
      }
    }

  usleep(2.0*1e6);

  // 6. Finally, try to move a tiny bit down. The gripper is still above the table but it is unreachable because of the padding:
  srv_2.request.pose_stamped.header.stamp = ros::Time::now();
  srv_2.request.pose_stamped.pose.position.x = 1.751;
  srv_2.request.pose_stamped.pose.position.y = -1.074;
  srv_2.request.pose_stamped.pose.position.z = 0.360;
  srv_2.request.pose_stamped.pose.orientation.x = 0.258;
  srv_2.request.pose_stamped.pose.orientation.y = 0.375;
  srv_2.request.pose_stamped.pose.orientation.z = -0.626;
  srv_2.request.pose_stamped.pose.orientation.w = 0.633;

    if (client_2.call(srv_2)) {
      if( !srv_2.response.result ) {
        ROS_INFO("SUCCESS: Pose is unreachable in account of the current padding of 2cm" );
      } else {
        ROS_INFO("FAILURE: This pose should be unreachable!" );
        return 1;
      }
    }




  return 0;
}
