
#include <craftsman_testing/testing_tracik_search.h>


/**
 * @brief Constructor
 */
TestingTracikSearch::TestingTracikSearch( ros::NodeHandle &_nh,
					  const std::string &_traj_cmd_topic ) :
  nh_( _nh ){
  topic_traj_cmd_ = _traj_cmd_topic;
  pub_traj_cmd_ = nh_.advertise<trajectory_msgs::JointTrajectory>( "/joint_path_command", 1 );

  // Planner node / RIT services
  srv_get_robot_state_ = nh_.serviceClient<robot_interaction_tools_msgs::GetRobotState>( "/planner_node/get_robot_state" );

  srv_send_stored_pose_ = nh_.serviceClient<robot_interaction_tools_msgs::SendStoredPose>( "/interactive_controls/send_stored_pose" );

  
  client_cartesian_ = new actionlib::SimpleActionClient<robot_interaction_tools_msgs::PlanCartesianAction>("planner_node/cartesian_planner", true); 
  client_cartesian_->waitForServer();
}


/**
 * @function turn_collision
 */
void TestingTracikSearch::turn_collision( bool _on ) {
  nh_.setParam("/planner_node/collision_check_override", _on );
}

/**
 * @function send_stored_pose
 */
bool TestingTracikSearch::send_stored_pose( std::string _group,
					    std::string _name ) {

  robot_interaction_tools_msgs::SendStoredPose srv;
  srv.request.group_name = _group;
  srv.request.pose_name = _name;

  if( srv_send_stored_pose_.call(srv) ) {
    return srv.response.result;
  } else {
    return false;
  }
}

/**
 * @function move_cartesian
 */
bool TestingTracikSearch::move_cartesian( std::string _group,
					  geometry_msgs::PoseStamped _pose ) {

  sensor_msgs::JointState current_pose;
  get_current_pose( current_pose, _group );
  ros::Time time_now = ros::Time::now();
  
  // Set cartesian 
  robot_interaction_tools_msgs::PlanCartesianGoal goal;
  
  goal.execute = true;
  goal.timeout = ros::Duration(10);
  goal.animate = false;
  robot_interaction_tools_msgs::GroupState start_state;
  start_state.name = _group;
  start_state.joints = current_pose;
  goal.start_states.push_back( start_state );
 
  robot_interaction_tools_msgs::CartesianPlanningGoalArray goal_array;
  goal_array.group_name = _group;
  robot_interaction_tools_msgs::CartesianPlanningGoal g1;
  
  g1.type = robot_interaction_tools_msgs::CartesianPlanningGoal::CARTESIAN;
  g1.conditioning_metric = robot_interaction_tools_msgs::CartesianPlanningGoal::MIN_DISTANCE;
  geometry_msgs::Vector3 zeros;zeros.x = 0; zeros.y=0; zeros.z=0;
  g1.position_tolerance_bounds.push_back(zeros);
  g1.position_tolerance_bounds.push_back(zeros); // min/max

  g1.orientation_tolerance_bounds.push_back(zeros);
  g1.position_tolerance_bounds.push_back(zeros); // min/max
  
  // Get pose from planning_frame to control_frame
  g1.offset.orientation.w = 1;
  g1.goal = _pose;

  goal_array.goals.push_back(g1);


  goal.goals.push_back(goal_array);
  client_cartesian_->sendGoal(goal);
  client_cartesian_->waitForResult( ros::Duration(10.0) );
  return (client_cartesian_->getState() == actionlib::SimpleClientGoalState::SUCCEEDED);    
}



/**
 * @function get_current_pose
 */
bool TestingTracikSearch::get_current_pose( sensor_msgs::JointState &_current_pose,
					    std::string _group_name ) {

  robot_interaction_tools_msgs::GetRobotState srv;
  srv.request.group = _group_name;

  if( !srv_get_robot_state_.call( srv ) ) { return false; }

  _current_pose = srv.response.state;
  return true;
}


/**
 * @function move_directly_to_joint_pose
 */
bool TestingTracikSearch::move_directly_to_joint_pose( Eigen::VectorXd _pose ) {

  sensor_msgs::JointState joint_msg;  
  if( !get_current_pose( joint_msg, "arm" ) ) { return false; }


  // Send trajectory msg
  trajectory_msgs::JointTrajectory traj_msg;
  traj_msg.header.stamp = ros::Time::now();
  traj_msg.header.seq = 0;
  traj_msg.joint_names = joint_msg.name;
  printf("Joint names size: %d \n", traj_msg.joint_names.size() );
  trajectory_msgs::JointTrajectoryPoint point;
  point.positions = joint_msg.position;
  point.velocities = std::vector<double>( point.positions.size(), 0 );
  point.time_from_start = ros::Duration(0.0);
  traj_msg.points.push_back( point );

  printf("Point %d : ", traj_msg.points.size() );
  for( int i = 0; i < point.positions.size(); ++i ) {
    printf(" %f ", point.positions[i]);
  } printf("\n");
  
  // Add pose
  if( _pose.size() != point.positions.size() ) {
    ROS_WARN("Pose size is wrong: %d  vs %d \n",
	     _pose.size(), point.positions.size());
    return false;
  }
  for( int i = 0; i < _pose.size(); ++i ) {
    point.positions[i] = _pose(i);
  }
  point.time_from_start = ros::Duration(5.0);
  traj_msg.points.push_back( point );

  printf("Point %d : ", traj_msg.points.size() );
  for( int i = 0; i < point.positions.size(); ++i ) {
    printf(" %f ", point.positions[i]);
  } printf("\n");

  

  ROS_WARN("Send TRAJ CMD NOW! \n");
  pub_traj_cmd_.publish( traj_msg );
  ROS_WARN("END Send TRAJ CMD NOW! \n");
  return true;
}
