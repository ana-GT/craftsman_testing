
#include <btBulletCollisionCommon.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <interactive_markers/interactive_marker_server.h>
#include <visualization_msgs/InteractiveMarker.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>
#include <memory>

/**
 * @class TestBullet
 */
class TestBullet {

public:

  void processFeedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback ) {

  switch ( _feedback->event_type ) {

    case visualization_msgs::InteractiveMarkerFeedback::POSE_UPDATE:
      geometry_msgs::Pose pose = _feedback->pose;
      static tf::TransformBroadcaster br;
      tf::Transform transform;
      tf::poseMsgToTF( pose, transform );
      br.sendTransform(tf::StampedTransform(transform, ros::Time::now(),
					    "world",
					    target_frame_[_feedback->marker_name]));
      break;
      /*
    case visualization_msgs::InteractiveMarkerFeedback::MOUSE_DOWN:
      ROS_INFO_STREAM( s.str() << ": mouse down" << mouse_point_ss.str() << "." );
      break;
      */

  } // end switch
  marker_server_.applyChanges();
  }

  // init_marker
  void init_marker( visualization_msgs::InteractiveMarker &_m,
		    std::string _name,
		    std::string _frame_id = "world" ) {

    // create an interactive marker for our server
    _m.header.frame_id = _frame_id;
    _m.header.stamp=ros::Time::now();
    _m.name = _name;
    _m.description = "6DOF control";

    // create a grey box marker
    visualization_msgs::Marker box_marker;
    box_marker.type = visualization_msgs::Marker::CUBE;
    box_marker.scale.x = 0.05;
    box_marker.scale.y = 0.05;
    box_marker.scale.z = 0.05;
    box_marker.color.r = 0.8;
    box_marker.color.g = 0.8;
    box_marker.color.b = 0.8;
    box_marker.color.a = 1.0;

    // create a non-interactive control which contains the box
    visualization_msgs::InteractiveMarkerControl box_control;
    box_control.always_visible = true;
    box_control.markers.push_back( box_marker );

    // add the control to the interactive marker
    _m.controls.push_back( box_control );

    // create a control which will move the box
    // this control does not contain any markers,
    // which will cause RViz to insert two arrows
    visualization_msgs::InteractiveMarkerControl dof_control;

    dof_control.orientation.w = 1;
    dof_control.orientation.x = 1;
    dof_control.orientation.y = 0;
    dof_control.orientation.z = 0;
    dof_control.name = "rotate_x";
    dof_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
    _m.controls.push_back(dof_control);
    dof_control.name = "move_x";
    dof_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
    _m.controls.push_back(dof_control);

    dof_control.orientation.w = 1;
    dof_control.orientation.x = 0;
    dof_control.orientation.y = 1;
    dof_control.orientation.z = 0;
    dof_control.name = "rotate_z";
    dof_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
    _m.controls.push_back(dof_control);
    dof_control.name = "move_z";
    dof_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
    _m.controls.push_back(dof_control);

    dof_control.orientation.w = 1;
    dof_control.orientation.x = 0;
    dof_control.orientation.y = 0;
    dof_control.orientation.z = 1;
    dof_control.name = "rotate_y";
    dof_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
    _m.controls.push_back(dof_control);
    dof_control.name = "move_y";
    dof_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
    _m.controls.push_back(dof_control);

  }

  // Constructor
  TestBullet( ros::NodeHandle &_nh ) :
    nh_(_nh),
    marker_server_("simple_marker") {
    // world_1 and world_2 are the base links of the 2 robots we are moving
    init_marker( m1_, "m1", "world" ); target_frame_["m1"] = "world_1";
    init_marker( m2_, "m2", "world" ); target_frame_["m2"] = "world_2";

    marker_server_.insert( m1_, boost::bind(&TestBullet::processFeedback, this, _1) );
    marker_server_.insert( m2_, boost::bind(&TestBullet::processFeedback, this, _1) );
  
    marker_server_.applyChanges();

  }
protected:
  interactive_markers::InteractiveMarkerServer marker_server_;
  visualization_msgs::InteractiveMarker m1_, m2_;
  std::map<std::string, std::string> target_frame_;
  ros::NodeHandle nh_;

  // Bullet stuff
  //     std::unique_ptr<btCollisionWorld> bullet_world_; // used to be here
  std::unique_ptr<btCollisionConfiguration> bullet_collision_configuration_;
  std::unique_ptr<btCollisionDispatcher> bullet_dispatcher_;
  std::unique_ptr<btBroadphaseInterface> bullet_broad_phase_alg_;

  std::unique_ptr<btCollisionWorld> bullet_world_; // now it is here and works

};




/**
 * @function 
 */
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "city_of_stars" );
  ros::NodeHandle nh;
  TestBullet tb(nh);
  // Load urdfs for 2 objects

  ros::Rate r(30);
  while( ros::ok() ) {
    ros::spinOnce();
    r.sleep();
  }

}
